package pro.shoppingCart.cyj.ShoppingCart;

import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition(
        info = @Info(
                description = "Resource Documentation",
                version = "V12.0.12",
                title = "Awesome Resource API",
                contact = @Contact(
                   name = "Cyril Johnson", 
                   email = "cyril.j@prodapt.com"
                ),
                license = @License(
                   name = "Apache 2.0", 
                   url = "http://www.apache.org/licenses/LICENSE-2.0"
                )
        ),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS}
)
public interface UserApiDocumentationConfig {

}