package pro.shoppingCart.cyj.ShoppingCart.item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ItemsDao {
	
	private static List<Items> items = new ArrayList<>();
	
	private static int itemsCount = 3;
	
	static {
		items.add(new Items(1, "Toothpaste", 70, 7));
		items.add(new Items(2, "Tooth-brush", 50, 3));
		items.add(new Items(3, "Ketchup", 80, 9));
	}

	public List<Items> findAllItems() {
		return items;
	}
	
	public Items save(Items item) {
		if (item.getItemId() == null) {
			item.setItemId(++itemsCount);
		}
		items.add(item);
		return item;
	}

	public Items findOneItem(int id) {
		for (Items item : items) {
			if (item.getItemId() == id) {
				return item;
			}
		}
		return null;
	}
	
	public Items deleteByItemId(int id) {
		Iterator<Items> iterator = items.iterator();
		while (iterator.hasNext()) {
			Items item = iterator.next();
			if (item.getItemId() == id) {
				iterator.remove();
				return item;
			}
		}
		return null;
	}
	
}
