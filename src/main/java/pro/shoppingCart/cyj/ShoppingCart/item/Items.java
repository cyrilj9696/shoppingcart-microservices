package pro.shoppingCart.cyj.ShoppingCart.item;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;

@ApiModel(description="All Items Summary")
@Entity
public class Items {
	@Id
	@GeneratedValue
	private Integer itemId;
	private String itemName;
	private Integer itemPrice;
	private Integer itemLocation;
	
	public Items(Integer itemId, String itemName, Integer itemPrice, Integer itemLocation) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.itemLocation = itemLocation;
	}
	
	public Integer getItemId() {
		return itemId;
	}
	
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public Integer getItemPrice() {
		return itemPrice;
	}
	
	public void setItemPrice(Integer itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	public Integer getItemLocation() {
		return itemLocation;
	}
	
	public void setItemLocation(Integer itemLocation) {
		this.itemLocation = itemLocation;
	}

	@Override
	public String toString() {
		return "Items [itemId=" + itemId + ", itemName=" + itemName + ", itemPrice=" + itemPrice + ", itemLocation="
				+ itemLocation + "]";
	}
	
}
