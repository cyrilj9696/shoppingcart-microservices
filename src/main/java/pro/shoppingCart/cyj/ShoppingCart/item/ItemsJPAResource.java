package pro.shoppingCart.cyj.ShoppingCart.item;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class ItemsJPAResource {

	@Autowired
	private ItemsDao product;
	
	@Autowired
	private ItemsRepository itemRepository;

	@GetMapping("/jpa/availableItems")
	public List<Items> retrieveAllItems() {
		return itemRepository.findAll();
	}
	
	
	@GetMapping("/jpa/availableItems/{itemId}")
	public Resource<Items> retrieveItem(@PathVariable int itemId) {
		Optional<Items> item = itemRepository.findById(itemId);
		
		if(!item.isPresent())
			throw new ItemNotFoundException("id - "+ itemId + " is not in the cart");
		
		Resource<Items> resource = new Resource<Items>(item.get());
		
		ControllerLinkBuilder linkTo = 
				linkTo(methodOn(this.getClass()).retrieveAllItems());
		
		resource.add(linkTo.withRel("all-items"));
		
		return resource;
	}
	
	@DeleteMapping("/jpa/availableItems/{itemId}")
	public void deleteItem(@PathVariable int itemId) {
		Items item = product.deleteByItemId(itemId);
		
		if(item==null)
			throw new ItemNotFoundException("id - "+ itemId + " is not in the cart");		
	}
	
	@PostMapping("/jpa/availableItems")
	public ResponseEntity<Object> createItem(@Valid @RequestBody Items item) {
		Items savedItem = product.save(item);
		
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/{itemId}")
			.buildAndExpand(savedItem.getItemId()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
}
