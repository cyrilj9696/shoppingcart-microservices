package pro.shoppingCart.cyj.ShoppingCart.item;

import java.util.List;
import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class ItemsResource {

	@Autowired
	private ItemsDao product;

	@GetMapping("/availableItems")
	public List<Items> retrieveAllItems() {
		return product.findAllItems();
	}

	@GetMapping("/availableItems/{itemId}")
	public Items retrieveItem(@PathVariable int itemId) {
		Items item = product.findOneItem(itemId);
		
		if(item==null)
			throw new ItemNotFoundException("id - "+ itemId + " is not in the cart");
		
		return item;
	}
	
	@DeleteMapping("/availableItems/{itemId}")
	public void deleteItem(@PathVariable int itemId) {
		Items item = product.deleteByItemId(itemId);
		
		if(item==null)
			throw new ItemNotFoundException("id - "+ itemId + " is not in the cart");
	}
	
	@PostMapping("/availableItems")
	public ResponseEntity<Object> createItem(@RequestBody Items item){
		Items savedItem = product.save(item);
		
		URI location = ServletUriComponentsBuilder
			.fromCurrentRequest()
			.path("/{itemId}")
			.buildAndExpand(savedItem.getItemId()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}

}
